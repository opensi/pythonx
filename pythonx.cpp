// pythonx.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <Windows.h>
#include <cstring>
#include <io.h>

void GetParentDirectory(char *path, char *parent)
{
    char *ptr = path;
    char *ptr_temp = parent;
    int i = 0;
    int end_index = -1;

    while (*ptr != '\0')
    {
        *ptr_temp = *ptr;
        if (*ptr == '\\')
        {
            end_index = i;
        }
        i++;
        ptr++;
        ptr_temp++;
    }

    if (end_index != -1)
    {
        *(parent + end_index + 1) = '\0';
    }
    else
    {
        *parent = '\0';
    }
}

void UpdateScripts(const char *py_home)
{
    using namespace std;
    HANDLE hFind;
    WIN32_FIND_DATA findData;
    char buf[MAX_PATH];
    char in_path[MAX_PATH];
    char out_path[MAX_PATH];
    FILE *fp_in = NULL;
    FILE *fp_out = NULL;
    unsigned char read_buf[MAX_PATH];
    int ac = 0;
    int i = 0;
    char ch = 0;
    char tmp_buf[MAX_PATH];
    int len = 0;
    char py_exe_path[MAX_PATH];

    sprintf(py_exe_path, "%s%s", py_home, "python.exe");
    sprintf(buf, "%sscripts\\%s", py_home, "*.exe");
    hFind = FindFirstFile(buf, &findData);
    if (hFind == INVALID_HANDLE_VALUE)
    {
        printf("no exe file found in %s\n", buf);
        return;
    }
    do
    {
        // 忽略"."和".."两个结果 
        if (strcmp(findData.cFileName, ".") == 0 || strcmp(findData.cFileName, "..") == 0)
            continue;

        // 忽略目录
        if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            continue;

        // 打开输入文件
        sprintf(in_path, "%sscripts\\%s", py_home, findData.cFileName);
        fp_in = fopen(in_path, "rb");
        if (fp_in < 0)
        {
            printf("open file %s fail!\n", in_path);
            return;
        }

        // 打开输出文件
        sprintf(out_path, "%sscripts\\%s.tmp", py_home, findData.cFileName);
        fp_out = fopen(out_path, "wb");
        if (fp_out == NULL)
        {
            printf("open file %s fail!", out_path);
            fclose(fp_in);
            return;
        }

        while((ac = fread(read_buf, sizeof(unsigned char), sizeof(read_buf), fp_in)) != 0)
        {
            //printf("ac:%d,buf:%0x,%0x,%0x\n", ac, read_buf[0], read_buf[1], read_buf[2]);
            for (i = 0; i < ac; i++)
            {
                ch = *(read_buf + i);
                
                if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') 
                    || (ch >= '0' && ch <= '9')
                    || ch == '\\' || ch == '.' || ch == ':' || ch == ' ')
                {
                    if (len < sizeof(tmp_buf))
                    {
                        tmp_buf[len] = ch;
                        len++;
                    }
                }
                else
                {
                    if (len < sizeof(tmp_buf))
                    {
                        tmp_buf[len] = '\0';
                    }
                    else
                    {
                        tmp_buf[sizeof(tmp_buf) - 1] = '\0';
                    }
                    
                    if (len < MAX_PATH && len > 0 && strstr(tmp_buf, "python.exe") != NULL)
                    {
                        /* 判断是否为我们需要查找的字符串 */
                        tmp_buf[len] = '\0';
                        printf("match %s\n", tmp_buf);

                        fseek(fp_out, -1 * len , SEEK_CUR);
                        fwrite(py_exe_path, strlen(py_exe_path), 1, fp_out);
                    }
                    len = 0;
                }

                fwrite(read_buf + i, 1, 1, fp_out);
            }
        }

        //printf("%s\n", read_buf);
        fclose(fp_in);
        fclose(fp_out);

        // 修改输出文件名称
        remove(in_path);
        if (rename(out_path, in_path) == 0)
        {
            printf("update file %s success!\n", in_path);
        }
    } while (FindNextFile(hFind, &findData));
    printf("update scripts finished!\n");
}

int _tmain(int argc, _TCHAR* argv[])
{
    char exe_path[MAX_PATH];
    char work_dir[MAX_PATH];
    char python_exe_path[MAX_PATH];
    char buf[MAX_PATH];

    /* 获取可执行文件路径 */
    GetModuleFileNameA(0, exe_path, sizeof(exe_path));
    
    /* test */
    //sprintf(exe_path, "F:\\python\\python.exe");
    
    /* 获取可执行文件所在目录 */
    GetParentDirectory(exe_path, work_dir);
    
    /* 检查python.exe是否存在 */
    sprintf(buf, "%s%s", work_dir, "python.exe");
    if (access(buf, 0))
    {
        printf("%s not exist!\n", buf);
        return 0;
    }
    sprintf(buf, "%s%s", work_dir, "scripts\\");
    UpdateScripts(work_dir);
    
	return 0;
}


